/*
   SPDX-FileCopyrightText: 2019-2023 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#pragma once

#include <QString>
namespace KPIMTextEdit
{
namespace EmoticonUnicodeUtils
{
Q_REQUIRED_RESULT QString emojiFontName();
}
}
