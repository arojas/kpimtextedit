/*
 * SPDX-FileCopyrightText: 2015-2023 Laurent Montel <montel@kde.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include "kpimtextedit_export.h"
#include <QObject>
#include <memory>
class QCompleter;
class QTextEdit;
class QPlainTextEdit;

namespace KPIMTextEdit
{
/**
 * @brief The TextEditorCompleter class
 * @author Laurent Montel <montel@kde.org>
 */
class KPIMTEXTEDIT_EXPORT TextEditorCompleter : public QObject
{
    Q_OBJECT
public:
    explicit TextEditorCompleter(QTextEdit *editor, QObject *parent);
    explicit TextEditorCompleter(QPlainTextEdit *editor, QObject *parent);
    ~TextEditorCompleter() override;

    void setCompleterStringList(const QStringList &list);

    Q_REQUIRED_RESULT QCompleter *completer() const;

    void completeText();

    void setExcludeOfCharacters(const QString &excludes);

private:
    KPIMTEXTEDIT_NO_EXPORT void slotCompletion(const QString &text);
    class TextEditorCompleterPrivate;
    std::unique_ptr<TextEditorCompleterPrivate> const d;
};
}
